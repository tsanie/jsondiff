﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace JsonDiff
{
	/// <summary>
	/// MainWindow.xaml 的交互逻辑
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		internal static string File1;
		internal static string File2;

		JToken a;
		JToken b;

		private void Window_ContentRendered( object sender, EventArgs e )
		{
			//JToken a = JToken.Parse( @"{""version"":[2,3,4],""a"":2}" );
			//JToken b = JToken.Parse( @"{""version"":[2,4,5],""a"":2}" );

			string sa = File.ReadAllText( File1 );
			string sb = File.ReadAllText( File2 );
			if ( sa.StartsWith( "svdata=" ) )
				sa = sa.Substring( 7 );
			if ( sb.StartsWith( "svdata=" ) )
				sb = sb.Substring( 7 );

			a = JToken.Parse( sb );
			b = JToken.Parse( sa );

			checkSimple_Checked( null, null );
		}

		void FillTree( List<JsonDiffItem> results, IAddChild container )
		{
			foreach ( var item in results )
			{
				var border = new Border
				{
					CornerRadius = new CornerRadius( 4 ),
					Margin = new Thickness( 0 )
				};
				var leafNode = new StackPanel
				{
					Orientation = Orientation.Horizontal,
					Margin = new Thickness( 6, 3, 6, 3 )
				};
				leafNode.Children.Add( new TextBlock { Text = item.Name + "" } );
				border.Child = leafNode;

				string value;
				if ( item.Type == "Integer" )
					value = item.Value;
				else if ( item.Type == "String" )
					value = "\"" + item.Value + "\"";
				else
					value = null;

				if ( value == null )
				{
					leafNode.Children.Add( new TextBlock { Text = ": " + item.Value } );
					leafNode.Children.Add( new TextBlock
					{
						Text = "(" + item.Type + ")",
						FontSize = 9,
						Foreground = Brushes.Gray,
						Margin = new Thickness( 6, 0, 0, 0 ),
						VerticalAlignment = VerticalAlignment.Center
					} );
				}
				else
				{
					leafNode.Children.Add( new TextBlock { Text = ": " + value } );
				}

				switch ( item.DiffType )
				{
					case JsonDiffType.Add:
						border.Background = Brushes.LightGreen;
						break;

					case JsonDiffType.Remove:
						border.Background = Brushes.LightCoral;
						break;

					case JsonDiffType.Modify:
						{
							border.Background = Brushes.LightYellow;

							if ( item.Type == "Integer" )
								value = item.Value2;
							else if ( item.Type == "String" )
								value = "\"" + item.Value2 + "\"";
							else
								value = null;

							if ( value == null )
							{
								leafNode.Children.Add( new TextBlock { Text = " => " + item.Value2 } );
								leafNode.Children.Add( new TextBlock
								{
									Text = "(" + item.Type2 + ")",
									FontSize = 9,
									Foreground = Brushes.Gray,
									Margin = new Thickness( 6, 0, 0, 0 ),
									VerticalAlignment = VerticalAlignment.Center
								} );
							}
							else
							{
								leafNode.Children.Add( new TextBlock { Text = " => " + value } );
							}
						}
						break;

					default:
						border.Background = Brushes.White;
						break;
				}

				if ( item.Children == null )
				{
					container.AddChild( border );
				}
				else
				{
					var listNode = new TreeViewItem
					{
						IsExpanded = true // ( item.Type == "Object" ) || item.Children.Any( d => d.Type == "Object" || d.Type == "Array" )
					};
					listNode.Header = border;
					FillTree( item.Children, listNode );
					container.AddChild( listNode );
				}
			}
		}

		private void checkSimple_Checked( object sender, RoutedEventArgs e )
		{
			bool show = ( !checkSimple.IsChecked != false );

			Task.Run( () =>
			{
				List<JsonDiffItem> list = new List<JsonDiffItem>();
				JsonDiffFactory.CompareTree( a, b, "root", list, show );

				Dispatcher.BeginInvoke( (Action)( () =>
				{
					treeResult.Items.Clear();
					FillTree( list, treeResult );
				} ) );
			} );
		}
	}

	class TreeViewLineConverter : IValueConverter
	{
		public object Convert( object value, Type targetType,
		object parameter, System.Globalization.CultureInfo culture )
		{
			TreeViewItem item = (TreeViewItem)value;
			ItemsControl ic = ItemsControl.ItemsControlFromItemContainer( item );
			return ic.ItemContainerGenerator.IndexFromContainer( item ) == ic.Items.Count - 1;
		}

		public object ConvertBack( object value, Type targetType,
		object parameter, System.Globalization.CultureInfo culture )
		{
			return false;
		}
	}
}
