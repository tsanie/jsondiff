﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace JsonDiff
{
	/// <summary>
	/// App.xaml 的交互逻辑
	/// </summary>
	public partial class App : Application
	{
		private void Application_Startup( object sender, StartupEventArgs e )
		{
			if ( e.Args.Length >= 2 )
			{
				JsonDiff.MainWindow.File1 = e.Args[0];
				JsonDiff.MainWindow.File2 = e.Args[1];
			}
		}

		private void Application_DispatcherUnhandledException( object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e )
		{
			MessageBox.Show( MainWindow, e.Exception.ToString(), "异常", MessageBoxButton.OK, MessageBoxImage.Error );
			e.Handled = true;
		}
	}
}
