﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace JsonDiff
{

	class JsonDiffItem
	{
		public object Name { get; set; }
		public string Type { get; set; }
		public string Value { get; set; }
		public string Type2 { get; set; }
		public string Value2 { get; set; }
		public JsonDiffType DiffType { get; set; }

		public List<JsonDiffItem> Children { get; set; }
	}

	enum JsonDiffType
	{
		None = 0,
		Add,
		Remove,
		Modify
	}

	class JsonDiffFactory
	{
		public static bool CompareTree( JToken a, JToken b, object name, List<JsonDiffItem> results, bool showValue )
		{
			string typeA = ( a == null ) ? "Undefined" : a.Type.ToString();
			string typeB = ( b == null ) ? "Undefined" : b.Type.ToString();
			bool arrayA = ( typeA == "Array" );
			bool arrayB = ( typeB == "Array" );
			bool objA = arrayA || ( typeA == "Object" );
			bool objB = arrayB || ( typeB == "Object" );
			string aString = objA ? "" : ( ( a == null ) ? "null" : a.ToString() );
			string bString = objB ? "" : ( ( b == null ) ? "null" : b.ToString() );

			bool rval = true;

			var item = new JsonDiffItem { Name = name };
			if ( a == null )
			{
				item.DiffType = JsonDiffType.Add;
				item.Value = bString;
				item.Type = typeB;
			}
			else if ( b == null )
			{
				item.DiffType = JsonDiffType.Remove;
				item.Value = aString;
				item.Type = typeA;
			}
			else if ( typeA != typeB || ( ( a is JValue ) && ( b is JValue ) && !a.Equals( b ) ) )
			{
				item.DiffType = JsonDiffType.Modify;
				item.Value = aString;
				item.Type = typeA;
				item.Value2 = bString;
				item.Type2 = typeB;
			}
			else
			{
				item.DiffType = JsonDiffType.None;
				item.Value = aString;
				item.Type = typeA;
				rval = false;
			}

			if ( objA || objB )
			{
				var listNode = item.Children = new List<JsonDiffItem>();
				if ( arrayA && arrayB )
				{
					JArray ja = (JArray)a;
					JArray jb = (JArray)b;
					JToken tmp;

					for ( int i = 0; i < ja.Count; i++ )
					{
						var j = ja[i];
						JToken bnext;
						if ( j is JValue || ( ( tmp = j["api_id"] ) == null ) )
							bnext = ( jb.Count > i ) ? jb[i] : null;
						else
						{
							bnext = jb.FirstOrDefault( t => !( t is JValue ) && tmp.Equals( t["api_id"] ) );
						}

						rval |= CompareTree( j, bnext, i, listNode, showValue );
					}

					for ( int i = 0; i < jb.Count; i++ )
					{
						var j = jb[i];
						if ( j is JValue || ( ( tmp = j["api_id"] ) == null ) )
						{
							if ( i >= ja.Count )
								rval |= CompareTree( null, j, i, listNode, showValue );
						}
						else
						{
							if ( !ja.Any( t => !( t is JValue ) && ( tmp.Equals( t["api_id"] ) ) ) )
								rval |= CompareTree( null, j, i, listNode, showValue );
						}
					}
				}
				else
				{
					var keys = new List<object>();
					if ( a != null )
					{
						if ( a.Type == JTokenType.Array )
							for ( int i = 0; i < ( (JArray)a ).Count; i++ )
								keys.Add( i );
						else
							foreach ( JProperty t in a )
								keys.Add( t.Name );
					}

					if ( b != null )
					{
						if ( b.Type == JTokenType.Array )
							for ( int i = 0; i < ( (JArray)b ).Count; i++ )
							{
								int index = keys.IndexOf( i );
								if ( index >= 0 )
									keys.Insert( index + 1, i );
								else
									keys.Add( i );
							}
						else
							foreach ( JProperty t in b )
							{
								int index = keys.IndexOf( t.Name );
								if ( index >= 0 )
									keys.Insert( index + 1, t.Name );
								else
									keys.Add( t.Name );
							}
					}

					//keys.Sort( ( o1, o2 ) => o1.ToString().CompareTo( o2.ToString() ) );

					for ( int i = 0; i < keys.Count; i++ )
					{
						if ( i > 0 && keys[i].Equals( keys[i - 1] ) )
							continue;

						JToken anext = null, bnext = null;
						try
						{
							if ( objA ) anext = a[keys[i]];
						}
						catch
						{ }

						try
						{
							if ( objB ) bnext = b[keys[i]];
						}
						catch
						{ }

						rval |= CompareTree( anext, bnext, keys[i], listNode, showValue );
					}
				}

				if ( rval )
					results.Add( item );
			}
			else if ( showValue || rval )
				results.Add( item );

			return rval;
		}
	}
}
