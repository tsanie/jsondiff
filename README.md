# README #

[![Build status](https://ci.appveyor.com/api/projects/status/a5gojm9fk26srnmg?svg=true)](https://ci.appveyor.com/project/tsanie/jsondiff)

该工具是用来简易对比两个json对象的变化的。

**使用方法**: 拖拽两个json文本文件到exe上即可进行对比。

**颜色含义**: ***红色***代表已删除、***绿色***代表已添加、***淡黄色***代表已改变。

**效果图**:

![preview.png](https://bitbucket.org/repo/6XE6KE/images/2182459132-preview.png)